//general includes
#include <time.h>
#define _USE_MATH_DEFINES
#include <cmath>
#include <vector>

//Messages

//other resources

//OpenCV
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "droneModuleROS.h"
#include "communication_definition.h"
#include "control/Controller_MidLevel_controlModes.h"
#include "control/simpletrajectorywaypoint.h"

//ROS
#include "std_msgs/String.h"
#include "std_srvs/Empty.h"
#include "std_msgs/Int16.h"
#include "std_msgs/UInt16.h"
#include "geometry_msgs/PoseStamped.h"
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include "tf/transform_datatypes.h"
#include "opencv_apps/RotatedRect.h"
#include "opencv_apps/RotatedRectStamped.h"
#include "geometry_msgs/Polygon.h"
#include "opencv_apps/Rect.h"

//DroneMsgsROS
#include "droneMsgsROS/droneMissionPlannerCommand.h"
#include "droneMsgsROS/droneHLCommandAck.h"
#include "droneMsgsROS/droneTrajectoryControllerControlMode.h"
#include "droneMsgsROS/setControlMode.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneStatus.h"
#include "droneMsgsROS/droneManagerStatus.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/dronePositionTrajectoryRefCommand.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
#include "droneMsgsROS/dronePitchRollCmd.h"
#include "droneMsgsROS/droneYawRefCommand.h"
#include "droneMsgsROS/droneAltitudeCmd.h"
#include "droneMsgsROS/droneDAltitudeCmd.h"
#include "droneMsgsROS/droneDYawCmd.h"
#include "droneMsgsROS/points3DStamped.h"
#include "droneMsgsROS/vector3f.h"
#include "droneMsgsROS/droneMission.h"
#include "droneMsgsROS/droneTask.h"
#include "droneMsgsROS/obsVector.h"
#include "droneMsgsROS/askForModule.h"
#include "droneMsgsROS/visualObjectRecognized.h"
#include "droneMsgsROS/vectorVisualObjectsRecognized.h"

#include <boost/thread.hpp>


#define FREQ_SUB_MISSION_PLANNER    10.0
#define M_PI    3.14159265358979323846

class droneSubMissionPlannerROSMoudule : public DroneModule
{
public:
  droneSubMissionPlannerROSMoudule();
  ~droneSubMissionPlannerROSMoudule();


    bool init();
    void close();
    void readParameters();
    bool resetValues();
    bool runStart();
    void open(ros::NodeHandle & nIn, std::string ModuleName);

private:
    //droneMsgsROS paramaters
    droneMsgsROS::dronePositionRefCommandStamped  drone_position_reference;
    droneMsgsROS::droneSpeeds drone_speed_reference;
    droneMsgsROS::droneYawRefCommand droneYawToLookMsg;
    droneMsgsROS::dronePositionRefCommand dronePointToLookMsg;
    droneMsgsROS::dronePose   current_drone_position_reference;
    droneMsgsROS::droneSpeeds current_drone_speed_reference;
    droneMsgsROS::dronePose   last_drone_estimated_GMRwrtGFF_pose;
    droneMsgsROS::droneMissionPlannerCommand mission_planner_command_msg;
    droneMsgsROS::droneStatus last_drone_status_msg;
    droneMsgsROS::droneHLCommandAck droneHLCommAckMsg;
    droneMsgsROS::dronePositionRefCommand mission_point;
    droneMsgsROS::droneYawRefCommand droneYawRefCommandMsg;
    droneMsgsROS::points3DStamped mission_points_msg;
    droneMsgsROS::droneMission droneMission;
    droneMsgsROS::droneTask next_task;
    geometry_msgs::PoseStamped point_to_navigation_stack;
    sensor_msgs::Image image_from_front_camera;
    std_msgs::Int16 UAV_task_id_msg;


    bool haltMissionScheduler;
    bool flagDistanceToYaw, breakMission, ObjectSeenOnce, NewMissionReceived;
    bool goalReachedFromNavigationStack;
    bool targetLockedFromVisualServoing;
    bool navigation_stack_started;
    bool object_ROI_VS_reference_in_image;
    bool init_flag_global_mission_started;
    int id_task, droneId;
    double distanceToYaw;
    float area_width, area_height, area_left_corner_x, area_left_corner_y;
    std_msgs::String droneSpeechCommandMsg;
    std_msgs::Bool droneMissionStateMsg;
    int32_t mp_command;
    int32_t last_drone_manager_status_msg;
    ros::Time taskInitTime;
    std::vector<droneMsgsROS::vector3f> mission_points;
    std_msgs::Bool TargetFound;
    std_msgs::Int16 idDrone;
    opencv_apps::Rect area_info_msg;

    boost::mutex goalReachedFromNavigationStack_mutex;
    boost::timed_mutex send_new_task_mutex;


protected:

     //Publishers
     ros::Publisher mission_planer_pub;
     ros::Publisher drone_speeds_reference_publisher;
     ros::Publisher dronePositionRefCommandPubl;
     ros::Publisher drone_yaw_reference_publisher;
     ros::Publisher drone_yaw_to_look_publisher;
     ros::Publisher drone_point_to_look_publisher;
     ros::Publisher drone_mission_state_pub;
     ros::Publisher drone_speech_command_pub;
     ros::Publisher drone_gmp_target_found_ack_pub;
     ros::Publisher drone_goal_to_navigation_publ;
     ros::Publisher drone_object_recognized_ToGMP_publ;
     ros::Publisher drone_uav_task_id_publ;
     ros::Publisher drone_VS_measurement_not_found_publ;
     ros::Publisher drone_arm_command_publ;


     //Subscribers
     ros::Subscriber drone_status_subs;
     ros::Subscriber drone_manager_status_subs;
     ros::Subscriber droneHLCommAckSub;
     ros::Subscriber drone_estimated_pose_subs;
     ros::Subscriber drone_position_reference_subscriber;
     ros::Subscriber drone_speed_reference_subscriber;
     ros::Subscriber drone_gmp_area_info_subs;
     ros::Subscriber drone_gmp_mission_sub;
     ros::Subscriber drone_gmp_break_mission_sub;
     ros::Subscriber drone_gmp_yaw_ref_sub;
     ros::Subscriber drone_yaw_ref_command_sub;
     ros::Subscriber drone_obs_vector_sub;
     ros::Subscriber drone_obs_from_navigation_subs;
     ros::Subscriber drone_goal_from_navigation_subs;
     ros::Subscriber drone_target_locked_from_VS_subs;
     ros::Subscriber object_ROI_visual_servoing_reference_subs;
     //ros::Subscriber drone_visual_object_recognized_subs;


     //Services
     ros::ServiceClient moduleIsStartedClientSrv;

     void restoreVariables();
     void managerAckCallback(const droneMsgsROS::droneHLCommandAck::ConstPtr& msg);
     void droneGMPAreaInfoCallback(const opencv_apps::Rect::ConstPtr &msg);
     void droneCurrentPositionRefsSubCallback(const droneMsgsROS::dronePose::ConstPtr &msg);
     void droneCurrentSpeedsRefsSubCallback(const droneMsgsROS::droneSpeeds::ConstPtr &msg);
     void drone_estimated_GMR_pose_callback(const  droneMsgsROS::dronePose &msg);
     void droneCurrentManagerStatusSubCallback(const droneMsgsROS::droneManagerStatus::ConstPtr &msg);
     void droneGMPMissionCallback(const droneMsgsROS::droneMission &msg);
     void droneGMPBreakMissionCallback(const std_msgs::Bool &msg);
     void droneGMPYawToLookCommandCallback(const droneMsgsROS::droneYawRefCommand &msg);
     void droneYawRefCommandCallback(const droneMsgsROS::droneYawRefCommand::ConstPtr& msg);
     void droneObsVectorCallback(const droneMsgsROS::obsVector &msg);
     void droneObsFromNavigationStackCallback(const std_msgs::Bool &msg);
     void droneGoalFromNavigationStackCallback(const std_msgs::Bool &msg);
     void droneTargetLocokedFromVSCallback(const std_msgs::Bool &msg);
     void droneObjectROIVisualServoingReference(const opencv_apps::RotatedRectStamped::ConstPtr &msg);
     //void droneVisualObjectRecognizedCallback(const droneMsgsROS::visualObjectRecognized &msg);

     bool calculatePointDistance();
     bool monitortakingoff(), calculateTaskTimeDuration(), monitorflip();
     bool sendNewTask(), readNewTask();
     int sendMissionMonitor();
     void sendCommandValues();
     void sendTaskReady();
     void generateSafetyPoint(float rad);


};
