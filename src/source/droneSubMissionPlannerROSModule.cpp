#include "droneSubMissionPlannerROSModule.h"
using namespace std;

droneSubMissionPlannerROSMoudule::droneSubMissionPlannerROSMoudule() :
DroneModule(droneModule::active, 15.0)
{
  if(!init())
    cout<< "Error in Init" << endl;

   droneId = DroneModule::idDrone;
   cout << "DroneID" << droneId << endl;

  return;
}

droneSubMissionPlannerROSMoudule::~droneSubMissionPlannerROSMoudule()
{
    close();
    return;
}


bool droneSubMissionPlannerROSMoudule::init()
{
   droneHLCommAckMsg.ack = false;
   NewMissionReceived = false;
   breakMission = false;
   ObjectSeenOnce = false;
   goalReachedFromNavigationStack_mutex.lock();
   goalReachedFromNavigationStack = false;
   goalReachedFromNavigationStack_mutex.unlock();
   targetLockedFromVisualServoing = false;
   object_ROI_VS_reference_in_image = false;
   id_task = 0;

   return true;
}

void droneSubMissionPlannerROSMoudule::close()
{
   DroneModule::close();
   return;
}

bool droneSubMissionPlannerROSMoudule::resetValues()
{
  //TODO: Reset values if there are any
  if(!DroneModule::resetValues())
     return false;

  return true;
}

bool droneSubMissionPlannerROSMoudule::runStart()
{
  if(!DroneModule::run())
     return false;
   else
      droneMissionStateMsg.data = true;
      drone_mission_state_pub.publish(droneMissionStateMsg);

    return true;
}

void droneSubMissionPlannerROSMoudule::open(ros::NodeHandle &nIn, std::string ModuleName)
{

    DroneModule::open(nIn,ModuleName);

    //Pubishers
    mission_planer_pub                        = n.advertise<droneMsgsROS::droneMissionPlannerCommand>("droneMissionPlannerCommand", 1, true);
    drone_yaw_to_look_publisher               = n.advertise<droneMsgsROS::droneYawRefCommand>("droneYawToLook", 1);
    dronePositionRefCommandPubl               = n.advertise<droneMsgsROS::dronePositionRefCommand>("droneMissionPoint", 1, true);
    drone_mission_state_pub                   = n.advertise<std_msgs::Bool>("missionState",1, true);
    drone_speech_command_pub                  = n.advertise<std_msgs::String>("message_to_say",1,true);
    drone_gmp_target_found_ack_pub            = n.advertise<std_msgs::Int16>("globalMissionPlannerTargetFoundAck",1,true);
    drone_point_to_look_publisher             = n.advertise<droneMsgsROS::dronePositionRefCommand>("dronePointToLook",1);
    drone_goal_to_navigation_publ             = n.advertise<geometry_msgs::PoseStamped>("globalGoal", 1);
    drone_uav_task_id_publ                    = n.advertise<std_msgs::Int16>("UAV_task_id", 1, true);
    drone_VS_measurement_not_found_publ       = n.advertise<droneMsgsROS::vector3f>("VS_measurement_not_found", 1, true);
    drone_arm_command_publ                    = n.advertise<std_msgs::String>("perception_manager/arm_command", 1, true);

    //Subscribers
    droneHLCommAckSub                         = n.subscribe("droneMissionHLCommandAck", 1, &droneSubMissionPlannerROSMoudule::managerAckCallback,this);
    drone_estimated_pose_subs                 = n.subscribe("EstimatedPose_droneGMR_wrt_GFF", 1, &droneSubMissionPlannerROSMoudule::drone_estimated_GMR_pose_callback, this);
    drone_position_reference_subscriber       = n.subscribe("trajectoryControllerPositionReferencesRebroadcast", 1, &droneSubMissionPlannerROSMoudule::droneCurrentPositionRefsSubCallback, this);
    drone_speed_reference_subscriber          = n.subscribe("trajectoryControllerSpeedReferencesRebroadcast", 1, &droneSubMissionPlannerROSMoudule::droneCurrentSpeedsRefsSubCallback, this);
    drone_manager_status_subs                 = n.subscribe("droneManagerStatus", 1, &droneSubMissionPlannerROSMoudule::droneCurrentManagerStatusSubCallback,this);
    drone_gmp_mission_sub                     = n.subscribe("globalMissionPlannerMission",1,&droneSubMissionPlannerROSMoudule::droneGMPMissionCallback, this);
    drone_gmp_break_mission_sub               = n.subscribe("globalMissionPlannerBreakMissionAck",1,&droneSubMissionPlannerROSMoudule::droneGMPBreakMissionCallback, this);
    drone_yaw_ref_command_sub                 = n.subscribe("droneControllerYawRefCommand",1,&droneSubMissionPlannerROSMoudule::droneYawRefCommandCallback, this);
    drone_obs_vector_sub                      = n.subscribe("arucoObservation",1,&droneSubMissionPlannerROSMoudule::droneObsVectorCallback, this);
    drone_obs_from_navigation_subs            = n.subscribe("obstacle_found", 1, &droneSubMissionPlannerROSMoudule::droneObsFromNavigationStackCallback, this);
    drone_goal_from_navigation_subs           = n.subscribe("goal_reached", 1, &droneSubMissionPlannerROSMoudule::droneGoalFromNavigationStackCallback, this);
    drone_target_locked_from_VS_subs          = n.subscribe("visual_servoing/target_locked", 1, &droneSubMissionPlannerROSMoudule::droneTargetLocokedFromVSCallback, this);
    object_ROI_visual_servoing_reference_subs = n.subscribe("visual_servoing/reference", 1, &droneSubMissionPlannerROSMoudule::droneObjectROIVisualServoingReference, this);
    drone_gmp_area_info_subs                  = n.subscribe("area_info", 1, &droneSubMissionPlannerROSMoudule::droneGMPAreaInfoCallback, this);

    moduleIsStartedClientSrv = n.serviceClient<droneMsgsROS::askForModule>("moduleIsStarted");

    droneModuleOpened = true;
    init_flag_global_mission_started = true;
    droneMissionStateMsg.data = false;
    drone_mission_state_pub.publish(droneMissionStateMsg);


    return;

}

//---------------------------------------------------------------------------------------------------------------

//Calbacks for the Subscribers  ------------------------------------------------------------------------------------

void droneSubMissionPlannerROSMoudule::droneGMPAreaInfoCallback(const opencv_apps::Rect::ConstPtr &msg)
{
    area_height = msg->height;
    area_width = msg->width;
    area_left_corner_x = msg->x;
    area_left_corner_y = msg->y;
    cout<<"++++ Area Info Received! ++++"<<endl;
    cout<<"Area Left Corner "<<"<"<<"x: "<<area_left_corner_x<<" ; "<<"y: "<<area_left_corner_y<<">"<<endl;
    cout<<"Area Size(Width/Height): "<<"("<<area_width<<"/"<<area_height<<")"<<endl;

}


void droneSubMissionPlannerROSMoudule::droneGMPMissionCallback(const droneMsgsROS::droneMission &msg)
{

    NewMissionReceived = true;
    cout<<endl<<endl<<"+++++++++ MISSION RECEIVED +++++++++" << endl;
    droneMission = msg;

    droneMsgsROS::droneTask task;

    for(int i=0; i<droneMission.mission.size(); i++)
    {
        task = droneMission.mission[i];
        cout<<"Task ["<<i<<"]"<<endl;
        cout<<"task time: "<<task.time<<endl;
        cout<<"mpCommand: "<<task.mpCommand<<endl;
        cout<<"speech_name: "<<task.speech_name<<endl;
        cout<<"module_names: "<<endl;
        for(int j=0;j<task.module_names.size();j++)
          cout<<task.module_names[j]<<endl;
        cout<<"point:"<<"x: "<<task.point.x<<" ; "<<"y:"<<task.point.y<<" ; "<<"z: "<<task.point.z<<endl;
    }
    cout<<"+++++++++  +++++++++" <<endl<<endl;


    restoreVariables();
    sendNewTask();


    return;
}



void droneSubMissionPlannerROSMoudule::managerAckCallback(const droneMsgsROS::droneHLCommandAck::ConstPtr& msg)
{
    droneMsgsROS::askForModule srv;


    if(msg->ack == true)
    {
      cout << "recibo ack" << endl;


      sendCommandValues();
      sendTaskReady();

      ros::spinOnce();
      ros::Duration(1.0).sleep();
      ros::spinOnce();

      navigation_stack_started = false;
      srv.request.module_name = MODULE_NAME_NAVIGATION_STACK;
      moduleIsStartedClientSrv.call(srv);
      if(srv.response.ack)
      {
          cout<<"Supervisor: Module "<< MODULE_NAME_NAVIGATION_STACK <<" Started"<<endl;
          navigation_stack_started = true;
      }
      else
          cout<<"Supervisor: Module "<< MODULE_NAME_NAVIGATION_STACK <<" NOT Started"<<endl;

      int exit_monitor_mode = 0;

      // Unlock the timed mutex
      send_new_task_mutex.unlock();
      while(!exit_monitor_mode && ros::ok())
      {
          exit_monitor_mode = sendMissionMonitor();
         //ROS_INFO("Processing Mission!");
         ros::spinOnce();
         ros::Duration(0.05).sleep();

      }
      if (exit_monitor_mode == 1) //Exit because MONITOR is finished
      {
          if(id_task < droneMission.mission.size()-1)
          {
              NewMissionReceived = false;
              goalReachedFromNavigationStack_mutex.lock();
              goalReachedFromNavigationStack = false;
              goalReachedFromNavigationStack_mutex.unlock();
              id_task++;        //incrementing the task id to start a new task
              sendNewTask();
          }
          else
          {
              id_task = -1;
              std_msgs::Int16 task_id_mission_finished;
              task_id_mission_finished.data = id_task;
              drone_uav_task_id_publ.publish(task_id_mission_finished);
          }
      }


    }
    else
    {
      ROS_INFO("Oooops, False ack");
    }

    return;
}

bool droneSubMissionPlannerROSMoudule::sendNewTask()
{
    //Get the timed lock
//    boost::timed_mutex::scoped_lock guard(send_new_task_mutex);

    boost::system_time timeout = boost::get_system_time() +
                             boost::posix_time::milliseconds(5000);

    bool ret =  send_new_task_mutex.timed_lock(timeout);

    if(!ret)
        cout << "Timeout in ack from Manager of Actions" << endl;

    UAV_task_id_msg.data = id_task;
    drone_uav_task_id_publ.publish(UAV_task_id_msg);
    if(id_task < droneMission.mission.size()-1)
    {
        next_task = droneMission.mission[id_task + 1];
//        cout<<"Next Task ["<<id_task + 1<<"]"<<endl;
//        cout<<"mpCommand: "<<next_task.mpCommand<<endl;
    }

    cout << "I am starting a new task" << endl;

    mission_planner_command_msg.time = ros::Time::now();

    // Command Message //
    switch (droneMission.mission[id_task].mpCommand)
    {
    case droneMsgsROS::droneTask::HOVER:
        mission_planner_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::HOVER;
        break;
    case droneMsgsROS::droneTask::LAND:
        mission_planner_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::LAND;
        break;
    case droneMsgsROS::droneTask::LAND_AUTONOMOUS:
        mission_planner_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::LAND_AUTONOMOUS;
        break;
    case droneMsgsROS::droneTask::TAKE_OFF:
        last_drone_manager_status_msg = droneMsgsROS::droneManagerStatus::TAKINGOFF;
        mission_planner_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::TAKE_OFF;
        break;
    case droneMsgsROS::droneTask::SLEEP:
        //sendTaskReady();
        //cmdSent = "SLEEP";
        mission_planner_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::SLEEP;
        break;
    case droneMsgsROS::droneTask::MOVE_TRAJECTORY:
        mission_planner_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::MOVE_TRAJECTORY;
        break;
    case droneMsgsROS::droneTask::MOVE_SPEED:
        mission_planner_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::MOVE_SPEED;
        break;
    case droneMsgsROS::droneTask::MOVE_POSITION:
        mission_planner_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::MOVE_POSITION;
        break;
    case droneMsgsROS::droneTask::MOVE_VISUAL_SERVOING:
        mission_planner_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::MOVE_VISUAL_SERVOING;
        break;
    case droneMsgsROS::droneTask::MOVE_VISUAL_SERVOING_RL:
        mission_planner_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::MOVE_VISUAL_SERVOING_RL;
        break;
    case droneMsgsROS::droneTask::EMERGENCY:
        mission_planner_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::MOVE_EMERGENCY;
        break;
    case droneMsgsROS::droneTask::MOVE_FLIP_RIGHT:
        mission_planner_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_RIGHT;
        break;
    case droneMsgsROS::droneTask::MOVE_FLIP_LEFT:
        mission_planner_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_LEFT;
        break;
    case droneMsgsROS::droneTask::MOVE_FLIP_FRONT:
        mission_planner_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_FRONT;
        break;
    case droneMsgsROS::droneTask::MOVE_FLIP_BACK:
        mission_planner_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_BACK;
        break;
    default:
        mission_planner_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::UNKNOWN;
        break;
    }

    std::vector<std::string> modules_names;
    modules_names = droneMission.mission[id_task].module_names;
    mission_planner_command_msg.drone_modules_names = modules_names;

    mission_planer_pub.publish(mission_planner_command_msg);
}


void droneSubMissionPlannerROSMoudule::droneCurrentPositionRefsSubCallback(const droneMsgsROS::dronePose::ConstPtr &msg)
{
    current_drone_position_reference = (*msg);
    return;
}

void droneSubMissionPlannerROSMoudule::droneCurrentSpeedsRefsSubCallback(const droneMsgsROS::droneSpeeds::ConstPtr &msg)
{
    current_drone_speed_reference = (*msg);
    return;
}

void droneSubMissionPlannerROSMoudule::drone_estimated_GMR_pose_callback(const  droneMsgsROS::dronePose &msg)
{
    last_drone_estimated_GMRwrtGFF_pose  = msg;

    return;
}

void droneSubMissionPlannerROSMoudule::droneObsVectorCallback(const droneMsgsROS::obsVector &msg)
{
  //Take the reading from the aruco observations and execute task

  for(int i=0; i<msg.obs.size(); i++)
    {
      if((msg.obs[i].id == 27) && !ObjectSeenOnce)
    //if((msg.obs[i].id == 27 || msg.obs[i].id == 31 || msg.obs[i].id == 67 || msg.obs[i].id == 25) && !ObjectSeenOnce)
      {
      idDrone.data = droneId;
      drone_gmp_target_found_ack_pub.publish(idDrone);
      ObjectSeenOnce = true;
      }
    }
}

void droneSubMissionPlannerROSMoudule::droneGMPBreakMissionCallback(const std_msgs::Bool &msg)
{
  breakMission = msg.data;
  return;
}

void droneSubMissionPlannerROSMoudule::droneCurrentManagerStatusSubCallback(const droneMsgsROS::droneManagerStatus::ConstPtr &msg)
{
    last_drone_manager_status_msg = msg->status;

    switch(last_drone_manager_status_msg)
    {
        case droneMsgsROS::droneManagerStatus::HOVERING:
            cout<<"Manager Status: HOVERING"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::SLEEPING:
            cout<<"Manager Status: SLEEPING"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::HOVERING_VISUAL_SERVOING:
            cout<<"Manager Status: HOVERING VISUAL SERVOING"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::LANDING:
            cout<<"Manager Status: LANDING"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::LANDING_AUTONOMOUS:
            cout<<"Manager Status: LANDING AUTONOMOUS"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::LANDED:
            cout<<"Manager Status: LANDED"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::TAKINGOFF:
            cout<<"Manager Status: TAKINGOFF"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_MANUAL_ALTITUD:
            haltMissionScheduler = true;
            cout<<"Manager Status: MOVING MANUAL ALTITUD"<<endl;
            cout<<"halting the mission" << endl;
            while(haltMissionScheduler == true)
            {
                ros::Duration(1).sleep();
                ros::spinOnce();
            }
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_POSITION:
            cout<<"Manager Status: MOVING POSITION"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_SPEED:
            cout<<"Manager Status: MOVING SPEED"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_TRAJECTORY:
            cout<<"Manager Status: MOVING TRAJECTORY"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_VISUAL_SERVOING:
            cout<<"Manager Status: MOVING VISUAL SERVOING"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_MANUAL_THRUST:
            cout<< "Manager Status: MOVING_MANUAL_THRUST"<<endl;
            cout<< "halting the Mission"<< endl;
            while(haltMissionScheduler == true)
            {
                ros::Duration(1).sleep();
                ros::spinOnce();
            }
            break;

        case droneMsgsROS::droneManagerStatus::EMERGENCY:
            cout<<"Managet Status: EMERGENCY"<<endl;
            breakMission = true;
            cout<<"Stopping Mission"<<endl;
    }
}



void droneSubMissionPlannerROSMoudule::droneYawRefCommandCallback(const droneMsgsROS::droneYawRefCommand::ConstPtr &msg)
{
    droneYawRefCommandMsg.header=msg->header;
    droneYawRefCommandMsg.yaw=msg->yaw;

    flagDistanceToYaw=true;

    return;
}

//--------------------------------------------------------------------------------------------------------------------------------------------------

bool droneSubMissionPlannerROSMoudule::calculatePointDistance()
{

    /// Position distances! ///
    //Now is a square in x-y. it should be a circle!!
    std::vector<double> distanceToMissionPoint;
    //x
    distanceToMissionPoint.push_back(fabs(last_drone_estimated_GMRwrtGFF_pose.x-mission_point.x));
    //y
    distanceToMissionPoint.push_back(fabs(last_drone_estimated_GMRwrtGFF_pose.y-mission_point.y));
    //z
    distanceToMissionPoint.push_back(fabs(last_drone_estimated_GMRwrtGFF_pose.z-mission_point.z));


    if(flagDistanceToYaw)
    {

        // atan2: in the interval [-pi,+pi] radians
        distanceToYaw=atan2(sin(last_drone_estimated_GMRwrtGFF_pose.yaw)*cos(droneYawRefCommandMsg.yaw)-sin(droneYawRefCommandMsg.yaw)*cos(last_drone_estimated_GMRwrtGFF_pose.yaw),
                            sin(last_drone_estimated_GMRwrtGFF_pose.yaw)*sin(droneYawRefCommandMsg.yaw)+cos(last_drone_estimated_GMRwrtGFF_pose.yaw)*cos(droneYawRefCommandMsg.yaw));

    }

    //Check distances
    bool flagPositionAchieved=false;

    if(distanceToMissionPoint[0] <= 0.2  && distanceToMissionPoint[1] <= 0.2 && distanceToMissionPoint[2] <= 0.2)
    {
        flagPositionAchieved=true;
    }

    //Check yaw distances
    bool flagYawAchieved=false;


    if(flagDistanceToYaw && (distanceToYaw <= 15.0) )
    {
        flagYawAchieved=true;
    }

    //Finish task
    if(flagPositionAchieved && flagYawAchieved)
    {
        return true;
    }

    return false;

}

bool droneSubMissionPlannerROSMoudule::monitortakingoff()
{
    if (last_drone_manager_status_msg!=droneMsgsROS::droneManagerStatus::TAKINGOFF)
      return true;
    else
      return false;
}

bool droneSubMissionPlannerROSMoudule::calculateTaskTimeDuration()
{

     ros::Duration taskDuration;
     taskDuration=ros::Time::now()-taskInitTime;

     if(taskDuration.toSec()>=droneMission.mission[id_task].time)
     {
         return true;
     }

     return false;
}

bool droneSubMissionPlannerROSMoudule::monitorflip()
{
    if(last_drone_manager_status_msg == droneMsgsROS::droneManagerStatus::HOVERING)
      return true;

    return false;
}

void droneSubMissionPlannerROSMoudule::sendCommandValues()
{
    //Publishing Speech message
    if(droneMission.mission[id_task].speech_name.size() != 0)
    {
      droneSpeechCommandMsg.data = "Proxima tarea" + droneMission.mission[id_task].speech_name;
      drone_speech_command_pub.publish(droneSpeechCommandMsg);
    }

    //Publishing Points for Trajectory
    if(droneMission.mission[id_task].point.x == 0 && droneMission.mission[id_task].point.y == 0 && droneMission.mission[id_task].point.z == 0 )
    {
      cout << "No trajectory sent" << endl;
      if(droneMission.mission[id_task].mpCommand == droneMsgsROS::droneTask::SLEEP)
      {
          point_to_navigation_stack.header.frame_id = "map";
          point_to_navigation_stack.pose.position.x = last_drone_estimated_GMRwrtGFF_pose.x;
          point_to_navigation_stack.pose.position.y = last_drone_estimated_GMRwrtGFF_pose.y;
          point_to_navigation_stack.pose.position.z = last_drone_estimated_GMRwrtGFF_pose.z;
          point_to_navigation_stack.pose.orientation.x = 0;
          point_to_navigation_stack.pose.orientation.y = 0;
          point_to_navigation_stack.pose.orientation.z = 0;
          point_to_navigation_stack.pose.orientation.w = 1;
          drone_goal_to_navigation_publ.publish(point_to_navigation_stack);
      }
    }
    else
    {

        if(droneMission.mission[id_task].point.x == 0 && droneMission.mission[id_task].point.y == 0)
        {
//            cout<<"Move In Altitud"<<endl;
//            mission_point.x = last_drone_estimated_GMRwrtGFF_pose.x;
//            mission_point.y = last_drone_estimated_GMRwrtGFF_pose.y;
//            mission_point.z = droneMission.mission[id_task].point.z;
//            dronePositionRefCommandPubl.publish(mission_point);

            point_to_navigation_stack.header.frame_id = "map";
            if(droneMission.mission[id_task].mpCommand == droneMsgsROS::droneTask::MOVE_MANUAL_ALTITUD)
            {
                //            cout<<"Move In Altitud"<<endl;
                point_to_navigation_stack.pose.position.x = last_drone_estimated_GMRwrtGFF_pose.x;
                point_to_navigation_stack.pose.position.y = last_drone_estimated_GMRwrtGFF_pose.y;

            }
            else
            {
                point_to_navigation_stack.pose.position.x = droneMission.mission[id_task].point.x;
                point_to_navigation_stack.pose.position.y = droneMission.mission[id_task].point.y;
            }
            point_to_navigation_stack.pose.position.z = droneMission.mission[id_task].point.z;
            point_to_navigation_stack.pose.orientation.x = 0;
            point_to_navigation_stack.pose.orientation.y = 0;
            point_to_navigation_stack.pose.orientation.z = 0;
            point_to_navigation_stack.pose.orientation.w = 1;
            drone_goal_to_navigation_publ.publish(point_to_navigation_stack);

        }
        else
        {
//            mission_point.x = droneMission.mission[id_task].point.x;
//            mission_point.y = droneMission.mission[id_task].point.y;
//            mission_point.z = droneMission.mission[id_task].point.z;
//            dronePositionRefCommandPubl.publish(mission_point);

            point_to_navigation_stack.header.frame_id = "map";
            point_to_navigation_stack.pose.position.x = droneMission.mission[id_task].point.x;
            point_to_navigation_stack.pose.position.y = droneMission.mission[id_task].point.y;
            point_to_navigation_stack.pose.position.z = droneMission.mission[id_task].point.z;
            point_to_navigation_stack.pose.orientation.x = 0;
            point_to_navigation_stack.pose.orientation.y = 0;
            point_to_navigation_stack.pose.orientation.z = 0;
            point_to_navigation_stack.pose.orientation.w = 1;
            drone_goal_to_navigation_publ.publish(point_to_navigation_stack);
        }
    }


    tf::Quaternion q_tf;
    float yaw_to_turn;
    // YawToLook / PoinToLook
    switch(droneMission.mission[id_task].yawSelector)
      {
        case 0:
          break;
        case 1:
          yaw_to_turn = (droneMission.mission[id_task].yaw * M_PI)/180;
          q_tf = tf::createQuaternionFromRPY(0, 0, yaw_to_turn);
          q_tf = q_tf.normalize();
          cout<<"Turn (Yaw): "<<droneYawToLookMsg.yaw<<endl;
          cout<<"Turn (Quaternion): "<<q_tf.x()<<" ; "<<q_tf.y()<<" ; "<<q_tf.z()<<" ; "<<q_tf.w()<<endl;

          point_to_navigation_stack.header.frame_id = "map";
          point_to_navigation_stack.pose.position.x = last_drone_estimated_GMRwrtGFF_pose.x;
          point_to_navigation_stack.pose.position.y = last_drone_estimated_GMRwrtGFF_pose.y;
          point_to_navigation_stack.pose.position.z = last_drone_estimated_GMRwrtGFF_pose.z;


          point_to_navigation_stack.pose.orientation.x = q_tf.x();
          point_to_navigation_stack.pose.orientation.y = q_tf.y();
          point_to_navigation_stack.pose.orientation.z = q_tf.z();
          point_to_navigation_stack.pose.orientation.w = q_tf.w();
          drone_goal_to_navigation_publ.publish(point_to_navigation_stack);

//          droneYawToLookMsg.yaw = droneMission.mission[id_task].yaw;
//          drone_yaw_to_look_publisher.publish(droneYawToLookMsg);
          break;
        case 2:
          dronePointToLookMsg.x = droneMission.mission[id_task].pointToLook.x;
          dronePointToLookMsg.y = droneMission.mission[id_task].pointToLook.y;
          drone_point_to_look_publisher.publish(dronePointToLookMsg);
          break;
      }


    return;
}

void droneSubMissionPlannerROSMoudule::sendTaskReady()
{
   taskInitTime = ros::Time::now();
   return;
}

int droneSubMissionPlannerROSMoudule::sendMissionMonitor()
{
    int exit_monitor_mode = 0;
    if(NewMissionReceived)
    {
        NewMissionReceived = false;
        exit_monitor_mode = 2; //Exit because NEW MISSION has been Received
    }
    else
    {
        switch (droneMission.mission[id_task].mpCommand)
          {
            case droneMsgsROS::droneTask::HOVER:
              if(calculateTaskTimeDuration())
                exit_monitor_mode = 1; //Exit because MONITOR is finished
              break;
            case droneMsgsROS::droneTask::LAND:
              exit_monitor_mode = 1;
              break;
            case droneMsgsROS::droneTask::LAND_AUTONOMOUS:
                if(last_drone_manager_status_msg == droneMsgsROS::droneManagerStatus::LANDED)
                    exit_monitor_mode = 1;
                break;
            case droneMsgsROS::droneTask::TAKE_OFF:
              if(monitortakingoff())
                exit_monitor_mode = 1;
              break;
            case droneMsgsROS::droneTask::SLEEP:
              if(calculateTaskTimeDuration())
              {
                  std::cout<<"SLEEP Finished"<<std::endl;
                  if(next_task.mpCommand == droneMsgsROS::droneTask::MOVE_VISUAL_SERVOING ||
                          next_task.mpCommand == droneMsgsROS::droneTask::MOVE_VISUAL_SERVOING_RL)
                  {
                      if(object_ROI_VS_reference_in_image)
                      {
                          cout<<"object_ROI_VS_reference_in_image: "<<true<<endl;
                          exit_monitor_mode = 1;
                      }
                      else
                      {
                          droneMsgsROS::vector3f point_object_not_found_msg;
                          point_object_not_found_msg.x = last_drone_estimated_GMRwrtGFF_pose.x;
                          point_object_not_found_msg.y = last_drone_estimated_GMRwrtGFF_pose.y;
                          point_object_not_found_msg.z = last_drone_estimated_GMRwrtGFF_pose.z;
                          drone_VS_measurement_not_found_publ.publish(point_object_not_found_msg);
                      }
                  }
                  else
                      exit_monitor_mode = 1;
              }
              break;
            case droneMsgsROS::droneTask::MOVE_TRAJECTORY:
                if(!navigation_stack_started)
                {
                  if(calculatePointDistance())
                    exit_monitor_mode = 1;
                }
                else
                {
                    goalReachedFromNavigationStack_mutex.lock();
                    if(goalReachedFromNavigationStack)
                        exit_monitor_mode = 1;
                    goalReachedFromNavigationStack_mutex.unlock();
                }
                break;
            case droneMsgsROS::droneTask::MOVE_POSITION:
                if(!navigation_stack_started)
                {
                    if(calculatePointDistance())
                      exit_monitor_mode = 1;
                }
                else
                {
                    //cout<< "Monitoring MOVING_POSITION" <<endl;
                    goalReachedFromNavigationStack_mutex.lock();
                    if(goalReachedFromNavigationStack)
                        exit_monitor_mode = 1;
                    goalReachedFromNavigationStack_mutex.unlock();
                }
                break;
            case droneMsgsROS::droneTask::MOVE_VISUAL_SERVOING:
                if(targetLockedFromVisualServoing)
                {
                    std::cout<<"MOVE_VISUAL_SERVOING Finished"<<std::endl;
                    exit_monitor_mode = 1;
                }
                if(calculateTaskTimeDuration())
                {
                    std::cout<<"Release triggered by timeout"<<std::endl;
                    std_msgs::String arm_command_msg;
                    arm_command_msg.data = "Release";
                    drone_arm_command_publ.publish(arm_command_msg);
                    exit_monitor_mode = 1;
                }
                break;
            case droneMsgsROS::droneTask::MOVE_VISUAL_SERVOING_RL:
                if(targetLockedFromVisualServoing)
                {
                    std::cout<<"MOVE_VISUAL_SERVOING Finished"<<std::endl;
                    exit_monitor_mode = 1;
                }
                if(calculateTaskTimeDuration())
                {
                    std::cout<<"Release triggered by timeout"<<std::endl;
                    std_msgs::String arm_command_msg;
                    arm_command_msg.data = "Release";
                    drone_arm_command_publ.publish(arm_command_msg);
                    exit_monitor_mode = 1;
                }
                break;
            case droneMsgsROS::droneTask::MOVE_SPEED:
              cout << "Moving Speed in not being Monitored" << endl;
              break;
            case droneMsgsROS::droneTask::EMERGENCY:
              cout << "In Emergency" << endl;
              break;
            case droneMsgsROS::droneTask::MOVE_FLIP_RIGHT:
              if(monitorflip())
                exit_monitor_mode = 1;
              break;
            case droneMsgsROS::droneTask::MOVE_FLIP_LEFT:
              if(monitorflip())
              break;
            case droneMsgsROS::droneTask::MOVE_FLIP_FRONT:
              if(monitorflip())
              break;
            case droneMsgsROS::droneTask::MOVE_FLIP_BACK:
              if(monitorflip())
              break;

          }
    }
    return exit_monitor_mode;

}


void droneSubMissionPlannerROSMoudule::droneObsFromNavigationStackCallback(const std_msgs::Bool &msg)
{
    bool nav_obs_reached = msg.data;

    if(nav_obs_reached)
    {
        cout<<"Point in Obstacle!"<<endl;
        generateSafetyPoint(0.2);
        sendCommandValues();
    }
}

void droneSubMissionPlannerROSMoudule::droneGoalFromNavigationStackCallback(const std_msgs::Bool &msg)
{
    bool nav_goal_reached = msg.data;

    if(nav_goal_reached)
    {
        cout<<"Goal Reached!"<<endl;
        goalReachedFromNavigationStack_mutex.lock();
        goalReachedFromNavigationStack = true;
        goalReachedFromNavigationStack_mutex.unlock();
    }

}

void droneSubMissionPlannerROSMoudule::droneTargetLocokedFromVSCallback(const std_msgs::Bool &msg)
{
    bool target_locked = msg.data;

    if(target_locked)
    {
        cout<<"Visual Servoing -- Target Locked! --"<<endl;
        targetLockedFromVisualServoing = true;
        std_msgs::String arm_command_msg;
        arm_command_msg.data = "Release";
        drone_arm_command_publ.publish(arm_command_msg);
    }
}

void droneSubMissionPlannerROSMoudule::droneObjectROIVisualServoingReference(const opencv_apps::RotatedRectStamped::ConstPtr &msg)
{
    if((msg->rect.size.width > 0) && (msg->rect.size.height > 0))
        object_ROI_VS_reference_in_image = true;
    else
        object_ROI_VS_reference_in_image = false;
}


void droneSubMissionPlannerROSMoudule::restoreVariables()
{
    if(id_task == -1 || init_flag_global_mission_started)
        NewMissionReceived = false;
    droneHLCommAckMsg.ack = false;
    breakMission = false;
    ObjectSeenOnce = false;
    goalReachedFromNavigationStack_mutex.lock();
    goalReachedFromNavigationStack = false;
    goalReachedFromNavigationStack_mutex.unlock();
    targetLockedFromVisualServoing = false;
    object_ROI_VS_reference_in_image = false;
    init_flag_global_mission_started = false;
    id_task = 0;
}

void droneSubMissionPlannerROSMoudule::generateSafetyPoint(float rad)
{
    bool point_inside_area = false;
    float limit_y = area_left_corner_y - area_width;
    float limit_x = area_left_corner_x + area_height;
    float safety_point_x, safety_point_y;
    int start = 0;
    start = clock();
    srand (time(NULL));

    while(!point_inside_area)
    {
        if(last_drone_estimated_GMRwrtGFF_pose.x < area_left_corner_x)
        {
            safety_point_x = droneMission.mission[id_task].point.x;
            safety_point_y = droneMission.mission[id_task].point.y;
            break;
        }
        double prob = ((float)rand()/( ((float)RAND_MAX) +1));
        float angle = prob * 2 * M_PI;
        safety_point_x = droneMission.mission[id_task].point.x + rad*cos(angle);
        safety_point_y = droneMission.mission[id_task].point.y + rad*sin(angle);

        if(((safety_point_y < area_left_corner_y) && (safety_point_y > limit_y)) &&
                ((safety_point_x  > area_left_corner_x) && (safety_point_x < limit_x)))
        {
            cout<<"Safety Point Generated!"<<endl;
            point_inside_area = true;
        }
    }

    droneMission.mission[id_task].point.x = safety_point_x;
    droneMission.mission[id_task].point.y = safety_point_y;

}
