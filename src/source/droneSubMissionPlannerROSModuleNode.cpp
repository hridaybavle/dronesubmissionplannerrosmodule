#include "ros/ros.h"
#include "droneSubMissionPlannerROSModule.h"
#include "nodes_definition.h"


int main (int argc,char **argv)
{
    //ROS init
    ros::init(argc, argv, "droneSubMissionPlannerROSModule");
    ros::NodeHandle n;
    std::cout << "[ROSNODE] Starting" << "droneSubMissionPlannerROSModule" << std::endl;

    droneSubMissionPlannerROSMoudule MydroneSubMissionPlannerROSMoudule;
    MydroneSubMissionPlannerROSMoudule.open(n, "droneSubMissionPlannerROSModule");


    try
    {
        while(ros::ok())
        {
            ros::spin();
        }

        return 1;
    }
    catch (std::exception &ex)
    {
        std::cout<<"[ROSNODE] Exception :"<<ex.what()<<std::endl;
    }

    return 1;

}
