cmake_minimum_required(VERSION 2.8.3)

set(PROJECT_NAME droneSubMissionPlannerROSModule)
project(${PROJECT_NAME})

### Use version 2011 of C++ (c++11). By default ROS uses c++98
#see: http://stackoverflow.com/questions/10851247/how-to-activate-c-11-in-cmake
#see: http://stackoverflow.com/questions/10984442/how-to-detect-c11-support-of-a-compiler-with-cmake
#add_definitions(-std=c++0x)
#add_definitions(-std=c++03)
add_definitions(-std=c++11)

# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries


set(DRONE_SMP_ROSMODULE_SOURCE_DIR
	src/source) 
	
set(DRONE_SMP_ROSMODULE_INCLUDE_DIR
	src/include)

set(DRONE_SMP_ROSMODULE_SOURCE_FILES
        ${DRONE_SMP_ROSMODULE_SOURCE_DIR}/droneSubMissionPlannerROSModule.cpp
       )
 
set(DRONE_SMP_ROSMODULE_HEADER_FILES
        ${DRONE_SMP_ROSMODULE_INCLUDE_DIR}/droneSubMissionPlannerROSModule.h
       )


find_package(catkin REQUIRED
    COMPONENTS roscpp std_msgs droneMsgsROS lib_cvgutils droneModuleROS droneVisualMarkersLocalizer
               droneTrajectoryPlanner tf_conversions opencv_apps
)


catkin_package(
       CATKIN_DEPENDS roscpp std_msgs droneMsgsROS lib_cvgutils droneModuleROS droneVisualMarkersLocalizer
                      tf_conversions opencv_apps
  )


include_directories(${DRONE_SMP_ROSMODULE_INCLUDE_DIR})
include_directories(${catkin_INCLUDE_DIRS})

add_library(droneSubMissionPlannerROSModule ${DRONE_SMP_ROSMODULE_SOURCE_FILES} ${DRONE_SMP_ROSMODULE_HEADER_FILES})
add_dependencies(droneSubMissionPlannerROSModule ${catkin_EXPORTED_TARGETS})
target_link_libraries(droneSubMissionPlannerROSModule ${catkin_LIBRARIES})

add_executable(droneSubMissionPlannerROSModuleNode ${DRONE_SMP_ROSMODULE_SOURCE_DIR}/droneSubMissionPlannerROSModuleNode.cpp)
add_dependencies(droneSubMissionPlannerROSModuleNode ${catkin_EXPORTED_TARGETS})
target_link_libraries(droneSubMissionPlannerROSModuleNode droneSubMissionPlannerROSModule)
target_link_libraries(droneSubMissionPlannerROSModuleNode ${catkin_LIBRARIES})
